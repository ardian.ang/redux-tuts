import { connect } from "react-redux";
import { decrementAction } from "../actions";
function B({ counter, decrement }) {
  const decrementFn = () => {
    decrement(counter);
  };
  return (
    <>
      <h1> Halaman B</h1>
      <p>{counter}</p>
      <button onClick={decrementFn}>Min--</button>
    </>
  );
}

const mapStateToProps = (state) => ({ counter: state.counter });
const dispatchToProps = (dispatch) => ({
  decrement: (counter) => {
    if (counter > 0) {
      dispatch(decrementAction);
    }
  },
});

export default connect(mapStateToProps, dispatchToProps)(B);
