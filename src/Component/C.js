import { useState } from "react";
import { connect } from "react-redux";
import { incrementNum } from "../actions";

function C({ counter, setNumber }) {
  const [numInput, setNumInput] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    setNumber(numInput);
    setNumInput("");
  };
  return (
    <>
      <h1> Halaman C</h1>
      <p>{counter}</p>
      <form onSubmit={handleSubmit}>
        <input placeholder="set number here..." value={numInput} onChange={(evt) => setNumInput(evt.target.value)}></input>
        <button type="submit" value="Submit">
          Set Number
        </button>
      </form>
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    counter: state.counter,
  };
};

const mapDispatchToProps = (dispatch) => ({
  setNumber: (num) => {
    dispatch(incrementNum(parseInt(num)));
    // dispatch(incrementNum(num));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(C);
