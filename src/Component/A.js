import { connect } from "react-redux";
import { incrementAction } from "../actions";

function A({ counter, increment }) {
  return (
    <>
      <h1> Halaman A</h1>
      <p>{counter}</p>
      <button onClick={increment}>Add++</button>
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    counter: state.counter,
  };
};

const mapDispatchToProps = (dispatch) => ({
  increment: () => {
    dispatch(incrementAction);
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(A);
