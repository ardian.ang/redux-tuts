const incrementAction = {
  type: "counter/increment",
};

const decrementAction = {
  type: "counter/decrement",
};

const incrementNum = (num) => {
  return { type: "counter/incrementNum", payload: num };
};
export { incrementAction, decrementAction, incrementNum };
