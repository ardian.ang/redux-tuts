import { connect } from "react-redux";
// import { incrementNum } from "./actions";

import "./App.css";
import A from "./Component/A";
import B from "./Component/B";
import C from "./Component/C";
export default function App({ counter, incrementnum }) {
  return (
    <div className="App">
      <A />
      <B />
      <C />
      {/* <br></br> */}
      {/* <button onClick={incrementnum}>Add Counter N</button> */}
    </div>
  );
}

// const mapStateToProps = (state) => {
//   return {
//     counter: state.counter,
//   };
// };

// const mapDispatchToProps = (dispatch) => ({
//   incrementnum: () => {
//     let n = prompt("Mau increment berapa bang?", 1);
//     if (!n) {
//       n = 1;
//     }
//     dispatch(incrementNum(parseInt(n)));
//   },
// });
// export default connect(mapStateToProps, mapDispatchToProps)(App);
